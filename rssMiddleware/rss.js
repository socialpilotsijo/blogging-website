const RssParser = require("rss-parser");
const rssParser = new RssParser();
const axios = require("axios");
const Promise = require("bluebird");
const uuid = require("uuid");

const {
  saveFeedDataDb,
  getFeedDynamically,
  updateLatestFeedList,
  updateFeedCurrent,
  updateSingleFeedDynamically,
} = require("../db/feedCollectionDb");

const { insertOnePost } = require("../db/PostCollectionDb");

const feed = require("feed-validator/providers/feed");

const fetchFeedsAndUpdateDb = async () => {
  try {
    const matchBy = { status: "Active", current: "posted" };
    const projectBy = { fid: 1, feedLink: 1, latestFeed: 1 };
    const feedArr = await getFeedDynamically(matchBy, projectBy);

    // update code with bluebird with  send 10 req same time

    Promise.each(feedArr, (feedRecord) => feedUpdateHelper(feedRecord));

    // for (let feed of feedArr) {
    //   feedUpdateHelper(feed);
    // }
  } catch (err) {
    console.log("Error occured in fetchFeed", err.message);
  }
};

const feedUpdateHelper = async (feed) => {
  try {
    const feedLink = feed["feedLink"];
    let latestFeed = feed["latestFeed"];
    const fid = feed["fid"];
    const feedsFetched = await rssParser.parseURL(feedLink);
    const fetchedFeedItems = feedsFetched.items;
    const feedItems = fetchedFeedItems.slice(0, 5);
    const fetchedFeedArr = [];
    const fetchedGuid = [];
    const neededFeedInfo = ["title", "contentSnippet", "link"];
    let itemFlag = true;
    feedItems.forEach((item) => {
      //if guid not provided we are using item.link as the guid
      itemFlag = true;
      // Will check if the neededFeedInfo is existing in the feed if not we return
      neededFeedInfo.forEach((value) => {
        if (!item[value]) {
          itemFlag = false;
          return;
        }
      });
      if (!itemFlag) return;
      const guid = item.guid ? item.guid : item.link;
      const temp = {
        title: item.title,
        content: item.contentSnippet,
        link: item.link,
        guid,
      };
      // Will keep the fetched feeds
      fetchedFeedArr.push(temp);
      //Will keep the fetched feeds guid
      fetchedGuid.push(guid);
    });
    // console.log(latestFeed.map((item) => item.guid));
    // Will check if the fetched feed is already existing by guid
    let duplicateGuidFlag = false;
    if (latestFeed.length !== 0) {
      latestFeed.forEach((item) => {
        if (fetchedGuid.includes(item.guid)) {
          duplicateGuidFlag = true;
          return;
        }
      });
    }
    if (duplicateGuidFlag) {
      console.log("Duplication occured");
      return;
    }

    latestFeed = fetchedFeedArr;

    const result = await updateLatestFeedList(fid, latestFeed);
  } catch (err) {
    console.log("Error occured in update helper", err);
  }
};

const postFeed = async () => {
  try {
    const matchBy = { status: "Active", current: "ready" };
    const projectBy = { fid: 1, latestFeed: 1, current: 1, uid: 1 };
    const feedArr = await getFeedDynamically(matchBy, projectBy);
    // console.log("Printing feed array", feedArr);
    if (feedArr.length === 0) return;

    Promise.each(feedArr, (feedData) => postFeedOnFb(feedData));
    // for (let feedData of feedArr) {
    //   console.log(feedData);
    //   postFeedOnFb(feedData);
    // }
  } catch (err) {
    console.log("Error occured in postFeed", err.message);
  }
};

const postFeedOnFb = async (feedArr) => {
  try {
    console.log("Posting feed on facebook called");
    const pageId = process.env.pageId;
    const accessToken = process.env.accessToken;
    const latestFeed = feedArr["latestFeed"];
    const fid = feedArr["fid"];
    const uid = feedArr["uid"];

    //Clearing the ids of old post
    await updateSingleFeedDynamically({ fid }, { $set: { id: [] } });

    const promiseArr = [];
    latestFeed.forEach((item) => {
      const title = encodeURIComponent(item.title);
      const content = encodeURIComponent(item.content);
      const result = axios
        .post(
          `https://graph.facebook.com/${pageId}/feed?message=${title}:${content}&link=${item.link}&access_token=${accessToken}`,
          null
        )
        .then(async (res) => {
          const id = res.data.id;
          const title = item.title;
          const postedOn = new Date();
          //Add postedOn and id list in feedCollection and add Posted data into post collection
          await Promise.all([
            updateSingleFeedDynamically(
              { fid },
              { $set: { postedOn }, $push: { id: id } }
            ),
            insertOnePost({ pid: id, fid, title, postedOn, uid }),
          ]);
        })
        .catch((err) => {
          console.log("Some mishap happened", err.message);
        });
      promiseArr.push(result);
    });
    console.log("We are posting");
    const result = await Promise.all(promiseArr);
    console.log("Done posting");
    // await updateFeedCurrent(fid, "posted");
    const matchFeedBy = { fid };
    const updateFeedItem = {
      $set: { current: "posted", postedOn: new Date() },
    };
    console.log("\n\n");
    console.log(matchFeedBy, updateFeedItem);
    console.log("\n\n");
    const result1 = await updateSingleFeedDynamically(
      matchFeedBy,
      updateFeedItem
    );
    console.log(result1);
  } catch (err) {
    console.log("Error occured on posting on facebok", err.message);
  }
};

module.exports = { fetchFeedsAndUpdateDb, postFeed };
