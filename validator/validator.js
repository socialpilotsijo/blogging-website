// uid, name, mail, pwd, company, position, rigths, address, logo, createdON
// require("dotenv").config({ path: "../.env" });

const uuid = require("uuid");
const jwt = require("jsonwebtoken");
const isImageUrl = require("image-url-validator").default;

const {
  getUserByEmail,
  getInviterDetails,
  getMasterList,
  isEmailExist,
  checkIfMasterIsValid,
} = require("../db/UserCollectionDb");

const {
  isOwnerOfTheBlog,
  isTeamOfTheBlog,
  checkDuplicateTitle,
  checkDuplicateTitleForUpdate,
} = require("../db/BlogCollectionDb");

const {
  getInviteMaster,
  getBlogListByRid,
  inviteExistByRid,
  isInviteAcceptedOrRejected,
  getRidifInviteAlreadyExistAndPending,
} = require("../db/InviteCollectionDb");

const validateUser = async (ctx, next) => {
  try {
    const userFields = [
      "name",
      "email",
      "password",
      "address",
      "company",
      "logo",
    ];
    const user = JSON.parse(JSON.stringify(ctx.request.body));

    validateName(user["name"]);
    validateEmail(user["email"]);

    //email
    if (await isEmailExist(user["email"]))
      throw new Error("This email already exists");

    validatePassword(user["password"]);
    validateCompany(user["company"]);
    validateAddress(user["address"]);
    await validateLogoUrl(user["logo"]);

    //Removing unnecessary fields and trimming
    Object.keys(user).forEach((key, index) => {
      if (!userFields.includes(key)) {
        delete user[key];
        return;
      }
      if (typeof user[key] === "string") {
        user[key] = user[key].trim();
      }
    });

    //Adding position and rights
    user["rights"] = 1;
    user["position"] = "Owner";

    // adding uid and createdOn field
    const value = await modifyUserData(user);
    ctx.state.user = user;

    await next();
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

const validateWriter = async (ctx, next) => {
  try {
    const dataData = ["email", "mid", "rid"];

    const dataHeader = ctx.query.data;
    const data = jwt.verify(dataHeader, process.env.SECRET_KEY);
    const email = data["email"];
    const mid = data["mid"];
    const rid = data["rid"];

    //Checking if the header value is valid or not
    dataData.forEach((value) => {
      if (!data.hasOwnProperty(value))
        throw new Error(
          `Invite link is not valid. Value ${value} doesn't exist`
        );
    });

    // Check if the rid (invite) is no longer valid
    await validateInvite(data["rid"]);

    const result = await getBlogListByRid(data["rid"]);
    const blogList = result["blogList"];

    // If the user is already registered
    if (await isEmailExist(data["email"])) {
      ctx.body = {
        response: "Looks like you have already been registered please login",
      };
      return;
    }

    const userFields = ["name", "password", "address"];

    const user = JSON.parse(JSON.stringify(ctx.request.body));

    validateName(user["name"]);
    validatePassword(user["password"]);
    validateAddress(user["address"]);

    //Removing unecessary fields

    //Removing unnecessary fields and trimming
    Object.keys(user).forEach((key, index) => {
      if (!userFields.includes(key)) {
        delete user[key];
        return;
      }
      if (typeof user[key] === "string") {
        user[key] = user[key].trim();
      }
    });

    if (data) {
      user["email"] = data["email"];
      user["master"] = [data["mid"]];
      user["current"] = data["mid"];
      user["blogList"] = blogList;
      user["rid"] = data["rid"];
    }

    user["position"] = "Content Writer";
    user["rights"] = 2;

    // adding uid and createdOn field
    await modifyUserData(user);
    // ctx.state.user = user;

    // Creating data for sendig email
    const emailResponse = {};
    const masterDetails = await getInviteMaster(rid);
    emailResponse["accepted"] = "accepted";
    emailResponse["email"] = user["email"];
    emailResponse["senderEmail"] = masterDetails["senderEmail"];

    // ctx.state.emailResponse = emailResponse;

    Object.assign(ctx.state, { user, emailResponse });

    await next(ctx);
  } catch (err) {
    console.log("Error occured", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

const validateWriter2 = async (ctx, next) => {
  try {
    const userFields = [
      "email",
      "name",
      "password",
      "address",
      "mid",
      "master",
      "current",
    ];

    const user = JSON.parse(JSON.stringify(ctx.request.body));
    console.log("user", user);
    validateInvite(user["rid"]);
    const rid = user["rid"];
    validateName(user["name"]);

    validatePassword(user["password"]);
    validateAddress(user["address"]);

    //Removing unecessary fields
    for (let [key, value] of Object.entries(user)) {
      if (!userFields.includes(key)) {
        delete user[key];
      }
    }

    user["master"] = [user["mid"]];
    user["current"] = user["mid"];

    const emailResponse = {};
    const masterDetails = await getInviteMaster(rid);
    emailResponse["accepted"] = "accepted";
    emailResponse["email"] = user["email"];
    emailResponse["senderEmail"] = masterDetails["senderEmail"];

    console.log(emailResponse);

    ctx.state.emailResponse = emailResponse;

    delete user["mid"];
    user["position"] = "Content Writer";
    user["rights"] = 2;

    if (await isEmailExist(user["email"]))
      throw new Error("This email already exists");

    // adding uid and createdOn field
    await modifyUserData(user);
    ctx.state.user = user;

    await next(ctx);
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

const validateName = (name) => {
  if (isAlpha(name)) {
    // Name should be of length greater than 2
    if (name.length <= 2 || name.length >= 12) {
      throw new Error(
        "Name should be atleast three character and atmost twelve character long"
      );
    }
  } else {
    throw new Error(`Name should be string`);
  }
};

const validateEmail = (email) => {
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (!regex.test(email)) {
    throw new Error("Invalid Email format");
  }
};

const validatePassword = (str) => {
  const regex = /^(?!.* )(?=.*[a-z])(?=.*[A-Z])(?=.*[-+_!@#$%^&*.,?]).+$/;
  if (!regex.test(str)) {
    throw new Error(
      "Password should be a combination of atleast one lowercase, uppercase, number, and special character and no spaces"
    );
  }
  if (str.length < 6)
    throw new Error("Password lenght should be greater than 6");
};

const validateCompany = (company) => {
  if (isAlpha(company)) {
    // Company should be of length greater than 2
    if (company.length <= 2 || company.length >= 15) {
      throw new Error(
        "Company name should be atleast three character and atmost fifteen character long"
      );
    }
  } else {
    throw new Error(`Company name should be string`);
  }
};

const validateAddress = (address) => {
  if (isAlpha(address)) {
    // Address should be of length greater than 2
    if (address.length <= 2 || address.length >= 20) {
      throw new Error(
        "Address should be atleast three character and atmost twenty character long"
      );
    }
  } else {
    throw new Error("Address should be string");
  }
};

const validateInvite = async (rid) => {
  if (!(await inviteExistByRid(rid)))
    throw new Error("This invite is no longer available");

  if (await isInviteAcceptedOrRejected(rid))
    throw new Error("You have already responded to the invite");
};

const validatePosition = (position) => {
  const posArr = ["Owner", "Team"];
  if (!posArr.includes(position)) {
    throw new Error("Invalid position");
  }
};

const validateRights = (rights) => {
  if (!isNumber(rights)) {
    if (rights !== 1 && rights !== 2) throw new Error("Rights are not valid");
  }
};

const validateLogoUrl = async (logoUrl) => {
  if (!(await isImageUrl(logoUrl))) {
    throw new Error("Logo url is not correct");
  }
};

const modifyUserData = async (user) => {
  user["uid"] = uuid.v4();
  user["createdOn"] = new Date();
};

////////////////////////////////////////////////////////////////////////////////////////

const validateGetMyBlog = async (ctx, next) => {
  try {
    const uid = ctx.state.uid;
    const rights = ctx.state.rights;
    const userId = ctx.query.uid;
    const type = ctx.query.type;
    const findBy = {};

    const projectBy = {
      uid: 1,
      bid: 1,
      title: 1,
      content: 1,
      category: 1,
    };

    if (rights === 1) {
      //Is a master
      const temp = { uid };
      if (userId) {
        if (!(await checkIfMasterIsValid(userId, uid)))
          throw new Error(`${userId} is not a member of your team`);
        temp["team"] = userId;
      }
      Object.assign(findBy, temp);
      projectBy["team"] = 1;
    } else {
      //Is a team member
      const temp = { team: uid };
      if (userId) {
        if (!(await checkIfMasterIsValid(uid, userId)))
          throw new Error(`You are not a member of ${userId} team`);

        temp["uid"] = userId;
      } else {
        //If no userId(owner id) is provided use current master

        const current = ctx.state.current;
        temp["uid"] = current;
      }
      Object.assign(findBy, temp);
    }

    const typeArr = ["Medical", "Social Media", "Technology"];

    if (type) {
      // If not valid type
      if (!typeArr.includes(type))
        throw new Error(`Invalid type, ${type} not valid`);

      findBy["category.type"] = type;
    }

    // ctx.state.findBy = findBy;
    // ctx.state.projectBy = projectBy;
    Object.assign(ctx.state, { findby, projectBy });

    await next(ctx);
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

const validateGetBlogWithTeam = async (ctx, next) => {
  try {
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;

    const { uid, rights } = ctx.state;
    const teamDisplay = ctx.query.team;
    const matchBy = { uid };

    if (rights !== 1) throw new Error("You are not an owner");

    // If type is provided (category type)
    let type = ctx.query.type;
    const typeArr = ["Medical", "Social Media", "Technology"];

    if (type) {
      // If not valid type
      if (!typeArr.includes(type))
        throw new Error(`Invalid type, ${type} not valid`);
      matchBy["category.type"] = type;
    }

    // If team Uid is provided

    const tid = ctx.query.tid;
    if (tid) {
      // Checking if the tid is in users team
      if (!(await checkIfMasterIsValid(tid, uid)))
        throw new Error(`${tid} is not a member of your team`);

      matchBy["team"] = tid;
    }

    const project = {
      _id: 0,
      bid: 1,
      title: 1,
      content: 1,
      image: 1,
      uid: 1,
    };

    console.log(`TeamDisplay ${teamDisplay.toLowerCase()}`);
    if (teamDisplay.toLowerCase() === "true") {
      project["TeamDetails"] = {
        uid: 1,
        name: 1,
      };
    }

    //
    Object.assing(ctx.state, { matchBy, projectBy });
    // ctx.state.matchBy = matchBy;
    // ctx.state.project = project;

    await next(ctx);
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

const validateBlog = async (ctx, next) => {
  //title

  try {
    const blog = JSON.parse(JSON.stringify(ctx.request.body));
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;

    const { uid, rights } = ctx.state;
    const blogItems = ["title", "content", "image", "category"];

    //Triming the string values of blog and deleting the unwanted fields
    Object.keys(blog).forEach((key) => {
      if (typeof blog[key] === "string") {
        blog[key] = blog[key].trim();
      }
      if (!blogItems.includes(key)) delete blog[key];
    });

    if (rights === 1) blog["uid"] = uid;
    else {
      const master = await getMasterList(uid);
      blog["uid"] = master["current"];
      blog["team"] = [uid];
    }

    validateTitle(blog["title"]);
    await checkIfTitleAlreadyExist(blog["title"], blog["uid"]);

    validateContent(blog["content"]);
    await validateImageUrl(blog["image"]);

    if (blog["category"]) {
      if (blog["category"] === null) {
        delete blog["category"];
      } else {
        validateCategory(blog["category"]);
      }
    }

    modifyBlog(blog);
    ctx.state.blog = blog;
    await next(ctx);
  } catch (err) {
    console.log("Error occured", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

const validateUpdate = async (ctx, next) => {
  try {
    // const rights = ctx.state.rights;
    // const uid = ctx.state.uid;

    const { rights, uid } = ctx.state;
    const bid = ctx.query.bid;
    const updatableField = ["title", "content", "image"];
    const blog = JSON.parse(JSON.stringify(ctx.request.body));

    //Triming the string values of blog
    Object.keys(blog).forEach((key) => {
      if (typeof blog[key] === "string") {
        blog[key] = blog[key].trim();
      }
      if (!updatableField.includes(key)) delete blog[key];
    });

    if (blog["title"]) {
      //Validate title
      validateTitle(blog["title"]);
      let uidForValidation;
      if (rights === 1) uidForValidation = uid;
      else {
        const master = await getMasterList(uid);
        uidForValidation = master["current"];
      }
      await checkIfTitleAlreadyExistForUpdate(
        blog["title"],
        uidForValidation,
        bid
      );
    }

    if (blog["content"]) validateContent(blog["content"]);
    if (blog["image"]) validateImageUrl(blog["image"]);

    blog["editedOn"] = new Date();
    ctx.state.blog = blog;
    await next(ctx);
  } catch (err) {
    console.log("Error occured", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

const validateTitle = (title) => {
  if (!isAlpha(title)) {
    throw new Error("Title should be string");
  }
};

const checkIfTitleAlreadyExist = async (title, uid) => {
  // console.log(title, uid);
  // console.log(checkDuplicateTitle(title, uid));
  if (!(await checkDuplicateTitle(title, uid)))
    throw new Error("This title already exist");
};

// Will check every title except of the blog being updated
const checkIfTitleAlreadyExistForUpdate = async (title, uid, bid) => {
  // console.log(title, uid);
  // console.log(checkDuplicateTitle(title, uid));
  if (!(await checkDuplicateTitleForUpdate(title, uid, bid)))
    throw new Error("This title already exist");
};

const validateContent = (str) => {
  if (!isAlpha(str)) {
    throw new Error("Content should be string");
  } else {
    if (str.length < 10) {
      throw new Error("Content should be atleast of 11 letters");
    }
  }
};

const validateImageUrl = async (imageUrl) => {
  if (!(await isImageUrl(imageUrl))) {
    throw new Error("Image url is not correct");
  }
};

const validateCategory = (category) => {
  const ageArr = ["10-18", "18-25", "25-up"];
  const typeArr = ["Medical", "Social Media", "Technology"];

  if (category["age"]) {
    if (Array.isArray(category["age"])) {
      if (category["age"].length === 0) delete category["age"];
      else {
        const age = category["age"];
        //Trimming the age
        age.forEach((value, index) => (age[index] = age[index].trim()));
        for (let item of age) {
          if (!ageArr.includes(item))
            throw new Error(
              `Wrong age category. Age category should be ${ageArr}`
            );
        }
      }
    } else {
      throw new Error("Age range should be in array");
    }
  }

  if (category["type"]) {
    if (Array.isArray(category["type"])) {
      const type = category["type"];

      if (type.length === 0) delete category["type"];
      else {
        //Trimming type
        type.forEach((value, index) => (type[index] = type[index].trim()));
        for (let item of type) {
          if (!typeArr.includes(item))
            throw new Error(
              `Wrong type category. Should include any of ${typeArr}`
            );
        }
      }
    } else {
      throw new Error("Category should be in array");
    }
  }
};

const modifyBlog = (blog) => {
  blog["createdOn"] = new Date();
  blog["bid"] = uuid.v4();
};

////////////////////////////////////////////////////////////////////////////

const inviteValidation = async (ctx, next) => {
  try {
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;

    const { uid, rights } = ctx.state;
    // Checking if the user is owner
    if (rights !== 1) throw new Error("Only owner can send invitation");

    const inviteUser = JSON.parse(JSON.stringify(ctx.request.body));
    const inviterDetails = await getInviterDetails(uid);
    let blogList = inviteUser["blogList"];

    if (!inviteUser["email"]) throw new Error("Invitation has no user email");
    if (!inviteUser["blogList"]) throw new Error("Blog list is not provided");

    validateEmail(inviteUser["email"]);

    // Flag that checks if this user is already in our team (if already exist)
    inviteUser["isAlreadyInMyTeamFlag"] = false;

    //Flag that checks if the user is already invited
    inviteUser["isAlreadyInvited"] = false;

    // Flag that tells if the user already exist
    inviteUser["exists"] = false;

    if (await isEmailExist(inviteUser["email"])) {
      const user = await getUserByEmail(inviteUser["email"]);
      //Checking if the invited user is owner
      if (user["rights"] === 1)
        throw new Error("This user is already registered as Owner");
      else {
        const master = user["master"];
        inviteUser["exists"] = true;
        // Checking if the member is already in our team
        if (master.includes(uid)) {
          inviteUser["isAlreadyInMyTeamFlag"] = true;
        }
      }
    }

    // Making the blog an array if it is not an array or if it is null
    if (!Array.isArray(blogList)) {
      inviteUser["blogList"] = [blogList];
      blogList = inviteUser["blogList"];
    } else {
      //It is already an array

      //Removing duplicate items and null or undefined
      blogList = blogList.filter((value, index) => {
        if (value && blogList.indexOf(value) === index) return true;
        else return false;
      });

      //Check if the array
      if (blogList.length === 0)
        throw new Error("Blog List should not be empty");
    }

    // Validate if blog exist and are owned by the inviting owner
    for (let value of blogList) {
      if (!(await isOwnerOfTheBlog(uid, value))) {
        throw new Error(`You don't own the blog ${value}`);
      }
    }

    // If already team we only need blogList just for update
    if (inviteUser["isAlreadyInMyTeamFlag"]) {
      ctx.state.inviteUser = inviteUser;
      await next(ctx);
      return;
    }

    // Check if the user is already Invited
    const result = await getRidifInviteAlreadyExistAndPending(
      uid,
      inviteUser["email"]
    );

    if (result) {
      inviteUser["rid"] = result["rid"];
      const existingBlogList = result["blogList"];
      existingBlogList.forEach((value) => {
        if (!blogList.includes(value)) blogList.push(value);
      });
      inviteUser["isAlreadyInvited"] = true;
    } else {
      inviteUser["rid"] = uuid.v4();
    }

    inviteUser["uid"] = ctx.state.uid;
    inviteUser["name"] = inviterDetails["name"];
    inviteUser["company"] = inviterDetails["company"];
    inviteUser["senderEmail"] = inviterDetails["email"];
    inviteUser["accepted"] = "pending";
    inviteUser["logo"] = inviterDetails["logo"];
    ctx.state.inviteUser = inviteUser;
    await next(ctx);
  } catch (err) {
    console.log("Error occured", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

const validateTeamRemoval = async (ctx, next) => {
  try {
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;

    const { uid, rights } = ctx.state;
    const bid = ctx.query.bid;
    const body = ctx.request.body;
    const tid = body["tid"];

    if (rights !== 1) throw new Error("You are not an owner");
    if (!tid) throw new Error("tid should be provided");

    if (!(await isTeamOfTheBlog(tid, bid))) {
      throw new Error("This tid is not team member of this blog");
    }

    const teamRemoval = { bid, tid };
    ctx.state.teamRemoval = teamRemoval;
    await next(ctx);
  } catch (err) {
    console.log("Error occured", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured from validator",
      error: err.message,
    };
  }
};

////////////////////////////////////////////////////////////////////////////
const isAlpha = (str) => typeof str === "string";
const isNumber = (num) => typeof num === "number";

////////////////////////////////////////////////////////////////////////////

const validateSavingFeedLink = async (ctx, next) => {
  try {
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;

    const { uid, rights } = ctx.state;
    const body = ctx.request.body;
    const feedLink = body["feedLink"];
    if (rights !== 1) throw new Error("You are not an owner");
    if (!feedLink) throw new Error("Feed Link is not provided");
    const fid = uuid.v4();
    const feed = {
      fid,
      feedLink,
      status: "Active",
      uid,
      current: "posted",
      image: "false",
      latestFeed: [],
    };
    ctx.state.feed = feed;

    await next(ctx);
  } catch (err) {
    console.log(err.message);

    ctx.body = {
      msg: "Error occured",
      error: err.message,
    };
  }
};

////////////////////////////////////////////////////////////////////////////
module.exports = {
  validateUser,
  validateWriter,
  validateWriter2,
  validateBlog,
  validateUpdate,
  inviteValidation,
  validateTeamRemoval,
  validateGetBlogWithTeam,
  validateGetMyBlog,
  validateSavingFeedLink,
};
