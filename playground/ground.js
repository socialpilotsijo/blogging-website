const apple = {
  color: "red",
  shape: "round",
};

const orange = { ...apple };
console.log(orange);

const { color: redColor } = apple;
console.log(redColor);

// Use spread operator for duplicating object,
// Use destructuring wherever possible even in state
// Make a specific mail function (remove from token)
// Add error list instead of a single error (if title and category is not valid)

// console.log(new Date());

// const Promise = require("bluebird");

// const fun = (text) => console.log(text);

// const arr = ["hello", "how", "are", "you"];

// Promise.each(arr, (item) => fun(item));

/////////////////////////////////////////////////////////////////////////
// const FeedValidator = require("feed-validator");
// const validator = FeedValidator.va;
// const feedUrl = "https://www.reddit.com/.rss";

// validator.validateURL(feedUrl, (error, result) => {
//   if (error) {
//     console.error(error);
//     console.log("The feed is invalid.");
//   } else if (result.feed) {
//     console.log("The feed is valid!");
//     console.log(result.feed);
//   } else {
//     console.log("The feed is invalid.");
//   }
// });

/////////////////////////////////////////////////////////////////////////

// const og = require("open-graph");
// const url =
//   "https://www.reddit.com/r/JEENEETards/comments/114exx0/aisa_farewell_to_hum_sab_deserve_karte_hai_ಥಥ";
// //   "https://timesofindia.indiatimes.com/tv/news/telugu/bigg-boss-telugu-4-fame-divi-vadthya-is-all-smiles-posing-with-gangavva-after-a-long-while-see-pic/articleshow/98008985.cms";
// og(url, function (err, meta) {
//   console.log(meta);
// });

/////////////////////////////////////////////////////////////////////////

// let Parser = require("rss-parser");
// let parser = new Parser();

// (async () => {
//   let feed = await parser.parseURL(
//     "https://timesofindia.indiatimes.com/rssfeeds/-2128936835.cms"
//   );
//   // console.log(feed.title);

//   // feed.items.forEach((item) => {
//   //   console.log(item.title + ":" + item.link + "\n\n");
//   // });

//   let items = feed.items;
//   items = items.slice(0, 1);

//   // items.forEach((item) => {
//   //   console.log(item.title + ":" + item.link + "\n\n");
//   // });

//   console.log(items[0]["content"]);
// })();

/////////////////////////////////////////////////////////////////////////

// const http = require("http");
// const cron = require("node-cron");

// const hostname = "127.0.0.1";
// const port = 3000;

// const server = http.createServer((req, res) => {
//   res.statusCode = 200;
//   res.setHeader("Content-Type", "text/plain");
//   res.end("Hello World");
// });

// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);

//   cron.schedule("*/1 * * * * *", () =>
//     console.log("Task submitted successfully")
//   );
// });
