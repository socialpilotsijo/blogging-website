const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const json = require("koa-json");
const router = require("./router/router");
const render = require("koa-ejs");
const path = require("path");
const cron = require("node-cron");
const { postFeed, fetchFeedsAndUpdateDb } = require("./rssMiddleware/rss");

const dotenv = require("dotenv");
dotenv.config();

const server = new Koa();

server.use(bodyParser());
server.use(json());
server.use(router.routes()).use(router.allowedMethods());

// render(server, {
//   root: path.join(__dirname, "views"),
//   layout: "layout",
//   viewExt: "html",
//   cache: false,
//   debug: false,
// });

server.listen(3000, () => {
  console.log("Server is up and running at port 3000");

  // cron.schedule("0 */2 * * * *", () => {
  //   console.log("Fetching data and updating");
  //   fetchFeedsAndUpdateDb();
  //   console.log("Posting data");
  //   postFeed();
  // });
});
