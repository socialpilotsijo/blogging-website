// require("dotenv").config({ path: "../.env" });
// const dotenv = require("dotenv");
// dotenv.config();

// console.log(pageId, accessToken);
const jwt = require("jsonwebtoken");

const RssParser = require("rss-parser");
const rssParser = new RssParser();

const {
  createToken,
  createTokenForWriter,
} = require("../middleware/tokenMiddleware");

const {
  saveBlog,
  deleteAllBlogsByUid,
  deleteTeamFromAllBlog,
  getAllBlogByUid,
  getAllBlogByTeamUid,
  getAllBlogDynamicly,
  getAllBlogs,
  getBlogById,
  getBlogWithTeam,
  removeMemberFromEveryBlog,
  removeTeamFromBlogById,
  updateParticularBlog,
  deleteParticularBlog,
  addTeamInBlog,
  dropallpost,
} = require("../db/BlogCollectionDb");

const {
  addInvite,
  getInvitesByEmail,
  getInvitesById,
  getInviteMaster,
  acceptInviteById,
  deleteAllInviteByEmail,
  deleteBlogFromInvite,
  updateBlogListInInvite,
  deleteAllInviteByUid,
  dropallinvites,
} = require("../db/InviteCollectionDb");

const {
  registerUser,
  getUserByEmail,
  getUserById,
  getTeamWithBlogs,
  getTeamMembers,
  setCurrentToNull,
  setCurrentToNullForMid,
  deleteUserByUid,
  deleteMasterFromAllUser,
  addMaster,
  removeMaster,
  getMaster,
  setCurrentMaster,
  getMasterList,
  getAllMasterListWithName,
  getMasterListWithName,
  isEmailExist,
  checkIfCurrentIsValid,
  checkIfMasterIsValid,
  dropallusers,
} = require("../db/UserCollectionDb");

const {
  saveFeedDataDb,
  getFeedDynamically,
  updateLatestFeedList,
  updateFeedCurrent,
} = require("../db/feedCollectionDb");

const registerScw = async (ctx) => {
  try {
    const user = ctx.state.user;
    const uid = user["uid"];
    console.log(uid);
    const result = await registerUser(user);

    const token = createToken(user["uid"], user["email"], user["rights"]);
    ctx.body = {
      msg: "User registered succesfully",
      uid,
      token,
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const registerWriter = async (ctx) => {
  try {
    const user = ctx.state.user;
    const uid = user["uid"];
    const mid = user["current"];
    const email = user["email"];

    const blogList = user["blogList"];
    const rid = user["rid"];
    delete user["blogList"];
    delete user["rid"];

    const result = await registerUser(user);
    const result1 = await acceptInviteById(rid, "accepted");
    const result2 = await addTeamInBlog(uid, blogList);

    // const result1 = await acceptInviteTable(mid, email, "accepted");
    //Updating the inviteCollection

    ctx.body = {
      msg: "User registered succesfully",
      uid,
      token: createTokenForWriter(
        user["uid"],
        user["email"],
        user["rights"],
        user["current"]
      ),
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

// const getRegisterWriterLink = async (ctx) => {
//   const link = ctx.request.query.linkText;
//   if (link) {
//     ctx.body = {
//       response: link,
//     };
//     return;
//   }
//   ctx.body = {
//     msg: "Link not found",
//   };
// };

const loginRender = async (ctx) => {
  const email = ctx.query.email;
  await ctx.render("loginWriter", { email });
};

const loginUser = async (ctx) => {
  try {
    const email = ctx.request.body.email;
    const password = ctx.request.body.password;

    const user = await getUserByEmail(email);

    let response;
    if (!user) {
      throw new Error("Invalid credentials (e)");
    } else {
      if (user["password"] !== password) {
        throw new Error("Invalid credentials (p)");
      } else {
        if (user["rights"] === 1) {
          response = "Login successfull";
          const token = createToken(user["uid"], email, user["rights"]);
          ctx.body = {
            response,
            token,
          };
          return;
        } else {
          const invites = await getInvitesByEmail(email);

          response = "Login successfull";
          // console.log("printing current", user["current"]);

          // If there are no master left
          const token = createTokenForWriter(
            user["uid"],
            email,
            user["rights"],
            user["current"]
          );

          if (user["master"].length === 0) {
            if (invites.length !== 0) {
              ctx.body = {
                response,
                msg: "You currently don't have any master but you have inivtes please accept invites to continue using app",
                token,
                invites,
              };
              return;
            } else {
              throw new Error(
                "Sorry you are currently not part of any team, so you can't login"
              );
            }
          }

          // If the current is null (Owner deletes his account or you are removed from team)
          if (user["current"] === null) {
            ctx.body = {
              response,
              msg: "Please change your master",
              token,
              invites,
            };
            return;
          }

          ctx.body = {
            response,
            token,
            invites,
          };
        }
      }
    }
  } catch (err) {
    console.log("Error occured", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const saveInviteDetails = async (ctx) => {
  try {
    const inviteUser = ctx.state.inviteUser;

    if (inviteUser["isAlreadyInMyTeamFlag"]) {
      const user = await getUserByEmail(inviteUser["email"]);
      const tid = user["uid"];
      await addTeamInBlog(tid, inviteUser["blogList"]);

      ctx.body = {
        msg: "This user already exist in your team and has been added to the blog",
      };
      return;
    }

    // Deleting as no more needed
    delete inviteUser["isAlreadyInMyTeamFlag"];

    if (inviteUser["isAlreadyInvited"]) {
      await updateBlogListInInvite(inviteUser["rid"], inviteUser["blogList"]);
      console.log("blogList", inviteUser["blogList"]);

      ctx.body = {
        msg: "Invitation sent with newer updates",
      };
      return;
    }

    delete inviteUser["isAlreadyInvited"];

    const result = await addInvite(inviteUser);
    ctx.body = {
      msg: "Invite sent successfully",
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const loginWriter = async (ctx) => {
  try {
    const formBody = JSON.parse(JSON.stringify(ctx.request.body));
    const email = formBody["email"];
    const password = formBody["password"];

    const user = await getUserByEmail(email);

    let response;
    if (!user) {
      throw new Error("Invalid credentials (e)");
    } else {
      if (user["password"] !== password) {
        throw new Error("Invalid credentials (p)");
      } else {
        if (user["rights"] === 1) {
          response = "Login successfull";
          const token = createToken(user["uid"], email, user["rights"]);
          ctx.body = {
            response,
            token,
          };
          return;
        } else {
          const invites = await getInvitesByEmail(email);
          response = "Login successfull";
          const token = createToken(user["uid"], email, user["rights"]);
          ctx.body = {
            response,
            token,
            invites,
          };
        }
      }
    }
  } catch (err) {
    console.log("Error occured", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const acceptInvite = async (ctx, next) => {
  try {
    const rights = ctx.state.rights;
    if (rights === 1) throw new Error("You are senior content writer");
    const rid = ctx.query.rid;
    const uid = ctx.state.uid;
    const email = ctx.state.email;
    let current = ctx.state.current;

    const accepted = ctx.request.body.accepted.toLowerCase();
    console.log(accepted);
    if (!rid) throw new Error("No rid provided");

    if (accepted !== "accepted" && accepted !== "rejected")
      throw new Error("You can either accept or reject");

    const invite = await getInviteMaster(rid);

    if (invite === null) throw new Error("No such invite");

    if (invite["email"] !== email)
      throw new Error("You are not authorized to accept invitation");

    if (invite["accepted"] === "accepted" || invite["accepted"] === "rejected")
      throw new Error("You have already responded to this invite");

    await acceptInviteById(rid, accepted);

    const emailResponse = {};
    emailResponse["accepted"] = accepted;
    emailResponse["senderEmail"] = invite["senderEmail"];
    emailResponse["email"] = invite["email"];

    ctx.state.emailResponse = emailResponse;
    console.log(emailResponse);
    await next(ctx);

    if (accepted === "accepted") {
      response = "You have accepted the request";
      console.log(uid, invite["uid"]);
      await Promise.all([
        addMaster(uid, invite["uid"]),
        addTeamInBlog(uid, invite["blogList"]),
      ]);
      console.log(uid, invite["uid"]);
    } else response = "You have rejected the request";

    if (current === null) {
      await setCurrentMaster(uid, invite["uid"]);
      current = invite["uid"];
    }

    const token = createTokenForWriter(uid, email, rights, current);

    ctx.body = {
      response,
      token,
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const leaveTeam = async (ctx) => {
  try {
    const rights = ctx.state.rights;
    const uid = ctx.state.uid;
    const mid = ctx.query.mid;
    const current = ctx.state.current;

    if (!mid) throw new Error("mid is not provided");

    if (!(await checkIfMasterIsValid(uid, mid)))
      throw new Error(`${mid} is not you master`);

    const promiseArr = [
      removeMaster(uid, mid),
      removeMemberFromEveryBlog(mid, uid),
    ];

    // If the master is current we are setting current to null
    if (await checkIfCurrentIsValid(uid, mid))
      promiseArr.push(setCurrentToNull(uid));

    await Promise.all(promiseArr);
    ctx.body = {
      response: `You have quit ${mid} team`,
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const removeFromMyTeam = async (ctx) => {
  try {
    const uid = ctx.state.uid;
    const rights = ctx.state.rights;
    const tid = ctx.query.tid;
    if (!tid) throw new Error("Tid not provided");
    if (rights !== 1) throw new Error("You are not an Owner");
    if (!(await checkIfMasterIsValid(tid, uid)))
      throw new Error("Not a member of your team");

    await Promise.all([
      removeMaster(tid, uid),
      removeMemberFromEveryBlog(uid, tid),
    ]);

    // If the current is the master deleting the member from team
    if (await checkIfCurrentIsValid(tid, uid)) {
      console.log("Set current to null");
      const result = await setCurrentToNull(tid);
      console.log("Null result", result);
    }

    ctx.body = {
      msg: "User deleted from your team",
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
//blogs

const createBlog = async (ctx) => {
  try {
    const blog = ctx.state.blog;
    await saveBlog(blog);
    ctx.body = {
      response: "Blog published",
      bid: blog["bid"],
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const getAllBlogById = async (ctx) => {
  try {
    const uid = ctx.state.uid;
    const projectBy = ctx.state.projectBy;
    const findBy = ctx.state.findBy;

    const result = await getAllBlogDynamicly(findBy, projectBy);

    ctx.body = {
      msg: "Retrieved your blog",
      response: result,
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const showAllBlogs = async (ctx) => {
  try {
    const result = await getAllBlogs();
    console.log(result);
    ctx.body = {
      msg: "Retrived all blog",
      response: result,
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const getBlogByBid = async (ctx) => {
  try {
    const bid = ctx.query.bid;
    console.log("We are here in bid", bid);
    const result = await getBlogById(bid);
    if (result !== null) {
      ctx.body = {
        blog: result,
      };
    } else throw new Error("No such bid");
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const deleteBlogByBid = async (ctx) => {
  try {
    const bid = ctx.query.bid;
    const rights = ctx.state.rights;

    if (rights !== 1) throw new Error("Blog can only be deleted by owner");
    // await deleteParticularBlog(bid);
    // await deleteBlogFromInvite(bid);
    await Promise.all([deleteParticularBlog(bid), deleteBlogFromInvite(bid)]);
    ctx.body = {
      msg: "Blog deleted successfully",
    };
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const updateBlogByBid = async (ctx) => {
  try {
    const bid = ctx.query.bid;
    const blog = ctx.state.blog;
    const result = await updateParticularBlog(bid, blog);
    ctx.body = {
      msg: "Blog Updated successfully",
    };
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

//////////////////////////////////////////////////////////////////////////////////////////////

const dropAllUsers = async (ctx) => {
  try {
    const result = await dropallusers();
    ctx.body = {
      response: "All users deleted",
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const dropAllPost = async (ctx) => {
  try {
    const result = await dropallpost();
    ctx.body = {
      response: "All post deleted",
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const dropAllInivtes = async (ctx) => {
  try {
    const result = await dropallinvites();
    ctx.body = {
      response: "All invites deleted",
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};
/////////////////////////////////////////////////////////////////////////////////////////////////

const inviteEmail = async (ctx) => {
  try {
    const inviteUser = ctx.state.inviteUser;
    const result = await addInvite(inviteUser);

    ctx.body = {
      msg: "Invitation sent successfully",
    };
  } catch (err) {
    console.log("Error occured", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const getMyInvite = async (ctx) => {
  try {
    const rights = ctx.state.rights;
    if (rights === 2) {
      const email = ctx.state.email;
      const result = await getInvitesByEmail(email);
      ctx.body = {
        msg: "Retrieved your invites that you haven't accepted",
        response: result,
      };
      return;
    } else {
      const uid = ctx.state.uid;
      const result = await getInvitesById(uid);
      ctx.body = {
        msg: "Retrieved the invites that you have sent and not been accepted",
        response: result,
      };
    }
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const changeMaster = async (ctx) => {
  try {
    const rights = ctx.state.rights;
    if (rights === 1)
      throw new Error(
        "You cannot change master as you are senior content writer"
      );
    const uid = ctx.state.uid;
    const mid = ctx.query.mid;
    const email = ctx.state.email;
    if (!mid) throw new Error("Mid is not provided");
    const data = await getMaster(uid);
    const master = data["master"];
    if (master.includes(mid)) {
      await setCurrentMaster(uid, mid);
    } else {
      throw new Error("You don't have a master with this mid");
    }
    const token = createTokenForWriter(uid, email, rights, mid);
    ctx.body = {
      response: `Your current master has been changed to ${mid}`,
      token,
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const getAllMaster = async (ctx) => {
  try {
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;
    const { uid, rights } = ctx.state;
    if (rights !== 2) throw new Error("You are not a team member");
    const result = await getAllMasterListWithName(uid);
    ctx.body = {
      msg: "Retrieved your master list",
      response: result,
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const getCurrentMaster = async (ctx) => {
  try {
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;

    const { uid, rights } = ctx.state;
    if (rights !== 2) throw new Error("You don't have a master");
    const result = await getMasterListWithName(uid);
    const current = ctx.state.current;
    if (current === null) {
      ctx.body = {
        msg: "Please change your master",
        response: {
          result,
        },
      };
      return;
    }
    ctx.body = {
      response: {
        result,
      },
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const deleteMyself = async (ctx, next) => {
  try {
    // const rights = ctx.state.rights;
    // const uid = ctx.state.uid;
    // const email = ctx.state.email;
    const { uid, rights, email } = ctx.state;
    let promiseArr = [];
    if (rights === 1) {
      // await deleteMasterFromAllUser(uid);
      // await setCurrentToNullForMid(uid);
      // await deleteAllBlogsByUid(uid);
      // await deleteAllInviteByUid(uid);

      promiseArr.push(
        deleteMasterFromAllUser(uid),
        setCurrentToNullForMid(uid),
        deleteAllBlogsByUid(uid),
        deleteAllInviteByUid(uid)
      );
    } else {
      // await deleteTeamFromAllBlog(uid);
      // await deleteAllInviteByEmail(email);

      promiseArr.push(
        deleteTeamFromAllBlog(uid),
        deleteAllInviteByEmail(email)
      );
    }

    promiseArr.push(deleteUserByUid(uid));

    await Promise.all(promiseArr);

    ctx.body = {
      msg: "You have been deleted",
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const RegisterWriterFromLink = async (ctx, next) => {
  try {
    const dataHeader = ctx.query.data;
    const data = jwt.verify(dataHeader, process.env.SECRET_KEY);
    // const email = data["email"];
    // const mid = data["mid"];
    // const rid = data["rid"];
    // const blogList = data["blogList"];
    const { email, mid, rid, blogList } = data;
    const registerData = {
      email,
      mid,
      rid,
      blogList,
    };

    // If the user is already registered
    const isEmailExistFlag = await isEmailExist(email);
    if (isEmailExistFlag) {
      ctx.body = {
        response: "Looks like you have already been registered please login",
      };
      return;
    }

    ctx.state.registerData = registerData;

    console.log("registerData", ctx.state.registerData);
  } catch (err) {
    console.log("Error occured:", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const getRegisterWriterLink = async (ctx) => {
  ctx.body = {
    response: ctx.request.url,
  };
};

const getMyTeam = async (ctx) => {
  try {
    const uid = ctx.state.uid;
    const rights = ctx.state.rights;
    if (rights !== 1) throw new Error("You are not an owner");
    const users = await getTeamMembers(uid);

    if (users.length !== 0) {
      ctx.body = {
        msg: "Retrieved your team members",
        response: users,
      };
      return;
    } else {
      ctx.body = {
        msg: "You currently don't have any team members",
      };
    }
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const deleteMyTeam = async (ctx) => {
  try {
    const tid = ctx.query.tid;
    if (!tid) throw new Error("tid is not provided");
    const uid = ctx.state.uid;
  } catch (err) {
    console.log("Error occured:", err.message);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const removeTeamFromBlog = async (ctx) => {
  try {
    const teamRemoval = ctx.state.teamRemoval;
    const { tid, bid } = ctx.state.teamRemoval;
    await removeTeamFromBlogById(tid, bid);

    ctx.body = {
      msg: "Member removed from blog team",
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////////
// const axios = require("axios");

// const pageId = 107795138902068;
// const accessToken =
//   "EAAv0ydFFXzIBAOEEY9aUM8fJhDvZARTYjDCwXbBvYN1VHjmIZAwklCQxy8qsgtd9dXKmCesYNYkBVdkTfAv2Jfrl5iDsiNgDZCzGJNgLjjfFVwUJIJLPxWohb38tQ9qxulSo5KymwDYVzfspjsi55JbzZBcNws42cyipMYRgIAyBeGZBrqoayPjjXmOv9CcIZD";
// const postOnFacebook = async (ctx) => {
//   try {
//     const txt = ctx.request.body.txt;
//     const img = ctx.request.body.img;
//     console.log(txt, img);
//     // const url = `https://graph.facebook.com/${pageId}/photos?url=${img}&message=${txt}&access_token=${accessToken}`;
//     // console.log(url);
//     const result = await axios.post(
//       `https://graph.facebook.com/${pageId}/photos?url=${img}&message=${txt}&access_token=${accessToken}`,
//       null
//     );

//     ctx.body = {
//       response: "Posted",
//     };
//   } catch (err) {
//     console.log("Error occured:", err.message);

//     ctx.status = 400;
//     ctx.body = {
//       msg: "An error occured",
//       error: err.message,
//     };
//   }
// };

const axios = require("axios");

const postOnFacebook = async (ctx) => {
  try {
    const pageId = process.env.pageId;
    const accessToken = process.env.accessToken;
    // const rights = ctx.state.rights;
    // const bid = ctx.state.bid;

    const { bid, rights } = ctx.state;
    if (rights !== 1) throw new Error("You are not an owner");
    const blog = await getBlogById(bid);
    console.log(blog);
    const txt = blog["content"];
    const img = blog["image"];
    // const url = `https://graph.facebook.com/${pageId}/photos?url=${img}&message=${txt}&access_token=${accessToken}`;
    // console.log(url);
    const result = await axios.post(
      `https://graph.facebook.com/${pageId}/photos?url=${img}&message=${txt}&access_token=${accessToken}`,
      null
    );

    ctx.body = {
      response: "Posted",
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

// curl --location --request POST 'https://graph.facebook.com/107795138902068/photos?url=https://spaceplace.nasa.gov/gallery-space/en/NGC2336-galaxy.en.jpg&message=Posting this from graph api&access_token=EAAv0ydFFXzIBAJG2rZBAnS4NAhxBqAOKdbrOIA894piqNLQEgxJHRT1xRXBVKqzVC76ZAllpJxOEo6wmKOWaaZAaMDR9htl77lIRTX7QQEYgBiSWqfb7GrjdISCnNlGpxRXu0lCwDZAs7tA1OqTtXTuCC0XT7XpJrtQJ81sFInKimkQpQrnk9hajL3fCfXEJ2NEg5rKh4Sxl6n8ymfIn' \
// --header 'Content-Type: application/json' \
// --data-raw '{
//     "txt":"This is post from postman",
//     "img":"https://spaceplace.nasa.gov/gallery-space/en/NGC2336-galaxy.en.jpgg"

// }'

////////////////////////////////////////////////////////////////////////////////////////////////////

const getPromiseData = async (ctx) => {
  try {
    const bid = ctx.request.body.bid;
    const uid = ctx.request.body.uid;
    console.log(bid, uid);
    let blog = getBlogById(bid);
    let user = getUserById(uid);
    const result = await Promise.all([blog, user]);
    ctx.body = {
      result: result,
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const getBlogWithTeamFunction = async (ctx) => {
  try {
    const uid = ctx.state.uid;
    const matchBy = ctx.state.matchBy;
    const project = ctx.state.project;
    const result = await getBlogWithTeam(uid, matchBy, project);

    ctx.body = {
      result,
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const getTeamWithBlogFunction = async (ctx) => {
  try {
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;

    const { uid, rights } = ctx.state;
    const blogsDisplay = ctx.query.blog;
    const tid = ctx.query.uid;

    // Checking if the person is an owner
    if (rights !== 1) throw new Error("You are not an owner");

    const matchBy = {
      master: uid,
    };

    const projectBy = {
      uid: 1,
      name: 1,
    };

    // If tid is provided
    if (tid) {
      //Checking if the tid is in owner team
      if (!(await checkIfMasterIsValid(tid, uid)))
        throw new Error(`${tid} is not in your team`);

      matchBy["uid"] = tid;
    }

    if (blogsDisplay) {
      projectBy["BlogDetails"] = {
        bid: 1,
        title: 1,
      };
    }
    const result = await getTeamWithBlogs(uid, matchBy, projectBy);

    ctx.body = {
      result,
    };
  } catch (err) {
    console.log("Error occured:", err.message);

    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const saveFeedLink = async (ctx) => {
  try {
    const feed = ctx.state.feed;
    const result = await saveFeedDataDb(feed);
    ctx.body = {
      response: "Feed added successfully",
    };
  } catch (err) {
    console.log(err.message);

    ctx.body = {
      msg: "Error occured",
      error: err.message,
    };
  }
};

const fetchFeed = async (ctx) => {
  try {
    const fid = ctx.query.fid;
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;

    const { uid, rights } = ctx.state;
    if (rights !== 1) throw new Error("You are not an owner");
    if (!fid) throw new Error("fid is not provided");

    const matchBy = { fid, uid, status: "Active", current: "posted" };
    const projectBy = {
      fid: 1,
      feedLink: 1,
      status: 1,
      latestFeed: 1,
      current: 1,
    };
    const feedArr = await getFeedDynamically(matchBy, projectBy);
    if (feedArr.length === 0) throw new Error("No data found with this fid");

    const feed = feedArr[0];
    const feedLink = feed["feedLink"];

    if (feed["status"] !== "Active") throw new Error("Feed status not active");

    if (feed["current"] !== "posted") throw new Error("Cannot fetch right now");

    let latestFeed = feed["latestFeed"];
    const feedsFetched = await rssParser.parseURL(feedLink);
    const fetchedFeedItems = feedsFetched.items;
    const feedItems = fetchedFeedItems.slice(0, 5);
    console.log("Printing feed Item \n\n", feedItems[0]);
    const fetchedFeedArr = [];
    const fetchedGuid = [];
    const neededFeedInfo = ["title", "contentSnippet", "link"];
    let itemFlag = true;
    feedItems.forEach((item) => {
      //if guid not provided we are using item.link as the guid
      itemFlag = true;
      neededFeedInfo.forEach((value) => {
        if (!item[value]) {
          itemFlag = false;
          return;
        }
      });
      if (!itemFlag) return;
      const guid = item.guid ? item.guid : item.link;
      const temp = {
        title: item.title,
        content: item.contentSnippet,
        link: item.link,
        guid,
      };
      // Will keep the fetched feeds
      fetchedFeedArr.push(temp);
      //Will keep the fetched feeds guid
      fetchedGuid.push(guid);
    });
    console.log(latestFeed.map((item) => item.guid));
    // Will check if the fetched feed is already existing
    if (latestFeed.length !== 0) {
      latestFeed.forEach((item) => {
        if (fetchedGuid.includes(item.guid)) throw new Error("Already updated");
      });
    }
    latestFeed = fetchedFeedArr;

    const result = await updateLatestFeedList(fid, latestFeed);

    ctx.body = {
      msg: "Updated Latest feed",
    };
  } catch (err) {
    console.log(err.message);

    ctx.body = {
      msg: "Error occured",
      error: err.message,
    };
  }
};

const postFeedOnFacebook = async (ctx) => {
  try {
    // const uid = ctx.state.uid;
    // const rights = ctx.state.rights;
    const { uid, rights } = ctx.state;

    const fid = ctx.query.fid;

    const pageId = process.env.pageId;
    const accessToken = process.env.accessToken;
    if (rights !== 1) throw new Error("You are not an owner");
    const matchBy = { fid, uid, status: "Active", current: "ready" };
    console.log(matchBy);
    const projectBy = { latestFeed: 1, current: 1 };
    const feedArr = await getFeedDynamically(matchBy, projectBy);
    console.log(feedArr);
    if (feedArr.length === 0)
      throw new Error("No such feed exist or is either inactive");
    const latestFeed = feedArr[0]["latestFeed"];
    const promiseArr = [];
    latestFeed.forEach((item) => {
      const title = encodeURIComponent(item.title);
      const content = encodeURIComponent(item.content);
      const result = axios.post(
        `https://graph.facebook.com/${pageId}/feed?message=${item.title}:${item.content}&link=${item.link}&access_token=${accessToken}`,
        null
      );

      promiseArr.push(result);
    });
    const result = await Promise.all(promiseArr);
    await updateFeedCurrent(fid, "posted");

    ctx.body = {
      msg: "Posted on facebook",
    };
  } catch (err) {
    console.log(err);
    ctx.body = {
      msg: "Error occured",
      error: err.message,
    };
  }
};
//////////////////////////////////////////////////////////

const getComment = async (ctx) => {
  try {
    const accessToken = process.env.accessToken;
    const postId = ctx.query.pid;
    if (!postId) throw new Error("Pid is not provided");
    const response = await axios.get(
      `https://graph.facebook.com/${postId}/comments?access_token=${accessToken}`
    );

    console.log(
      `https://graph.facebook.com/${postId}/comments?access_token=${accessToken}`
    );

    const data = response["data"]["data"];

    console.log(data);

    // If there are no comments on the post
    if (data.length === 0) {
      ctx.body = {
        Response: "Post has no comments",
      };
      return;
    }

    const result = data.map((comment) => {
      const temp = {
        created_time: comment["created_time"],
        name: comment["from"]["name"],
        userId: comment["from"]["id"],
        message: comment["message"],
        commentId: comment["id"],
      };
      return temp;
    });

    ctx.body = {
      msg: "Retrieved comments",
      response: result,
    };
  } catch (err) {
    console.log(err.messsage);
    ctx.body = {
      msg: "Error occured",
      error: err.message,
    };
  }
};

const postComment = async (ctx) => {
  try {
    const postId = ctx.query.pid;
    const comment = ctx.request.body.comment;
    const accessToken = process.env.accessToken;

    if (!postId) throw new Error("Pid is not provided");
    if (!comment) throw new Error("Comment is not provided");

    await axios.post(
      `https://graph.facebook.com/${postId}/comments?message=${comment}&access_token=${accessToken}`,
      null
    );
    console.log(
      `https://graph.facebook.com/${postId}/comments?message=${comment}&access_token=${accessToken}`
    );

    ctx.body = {
      msg: "Comment posted",
    };
  } catch (err) {
    console.log(err.message);
    ctx.body = {
      msg: "Error occured",
      error: err.message,
    };
  }
};

const getReaction = async (ctx) => {
  try {
    const postId = ctx.query.pid;
    const accessToken = process.env.accessToken;
    const type = ctx.request.body.type;
    if (!postId) throw new Error("Pid is not provided");

    let link = `https://graph.facebook.com/${postId}/reactions?summary=true&access_token=${accessToken}`;

    if (type) link = link + `&type=${type}`;

    const response = await axios.get(link);

    console.log(link);
    const totalReaction = response.data["summary"]["total_count"];
    const yourReaction = response.data["summary"]["viewer_reaction"];

    const data = response.data["data"];

    const likeData = data.map((value) => {
      const temp = {
        name: value["name"],
        type: value["type"],
      };
      return temp;
    });

    const result = {
      totalReaction,
    };

    if (yourReaction !== "NONE") Object.assign(result, { yourReaction });

    if (data.length) result["data"] = likeData;

    ctx.body = {
      response: result,
    };
  } catch (err) {
    console.log(err.message);
    ctx.body = {
      msg: "Error occured",
      error: err.message,
    };
  }
};

const likePost = async (ctx) => {
  try {
    const postId = ctx.query.pid;
    const accessToken = process.env.accessToken;
    if (!postId) throw new Error("Pid is not provided");
    await axios.post(
      `https://graph.facebook.com/${postId}/likes?&access_token=${accessToken}`,
      null
    );

    ctx.body = {
      response: "Liked the content",
    };
  } catch (err) {
    console.log(err.message);
    ctx.bodu = {
      msg: "Error occured",
      error: err.message,
    };
  }
};

const postOnInstagram = async (ctx) => {
  const image_url = ctx.request.body.image;
  // const instaAccessToken =
};

module.exports = {
  registerScw,
  registerWriter,
  loginUser,
  deleteMyself,
  deleteMyTeam,
  removeTeamFromBlog,
  loginRender,
  createBlog,
  inviteEmail,
  getAllBlogById,
  getAllMaster,
  showAllBlogs,
  getBlogWithTeamFunction,
  getTeamWithBlogFunction,
  saveInviteDetails,
  deleteBlogByBid,
  updateBlogByBid,
  getBlogByBid,
  getMyInvite,
  getMyTeam,
  removeFromMyTeam,
  leaveTeam,
  acceptInvite,
  changeMaster,
  getCurrentMaster,
  dropAllPost,
  dropAllUsers,
  dropAllInivtes,
  getRegisterWriterLink,
  // getRegisterWriterLink2,
  loginWriter,
  postOnFacebook,
  getPromiseData,
  saveFeedLink,
  fetchFeed,
  postFeedOnFacebook,
  getComment,
  postComment,
  getReaction,
  likePost,
};

// aggregate([
//   {
//     $lookup: {
//       from: "users",
//       localField: "uid",
//       foreignField: "uid",
//       as: "UserDetails",
//     },
//   },
//   {
//       $project: {
//           _id:0,
//           uid:1,
//           bid:1,
//           title:1,
//           content:1,
//           UserDetails:{
//               name: 1,
//               company:1
//           }
//       }
//   },
//   {
//       $unwind: "$UserDetails"
//   },

// ])
