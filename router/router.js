require("dotenv").config({ path: "../.env" });
const KoaRouter = require("koa-router");
const {
  validateUser,
  validateBlog,
  validateUpdate,
  inviteValidation,
  validateWriter,
  validateWriter2,
  validateTeamRemoval,
  validateGetBlogWithTeam,
  validateGetMyBlog,
  validateSavingFeedLink,
} = require("../validator/validator");

const {
  registerScw,
  registerWriter,
  loginUser,
  createBlog,
  getAllBlogById,
  getBlogByBid,
  showAllBlogs,
  deleteBlogByBid,
  deleteMyTeam,
  deleteMyself,
  updateBlogByBid,
  saveInviteDetails,
  removeFromMyTeam,
  leaveTeam,
  getMyInvite,
  acceptInvite,
  changeMaster,
  getCurrentMaster,
  getAllMaster,
  loginRender,
  loginWriter,
  getMyTeam,
  getTeamWithBlogFunction,
  postOnFacebook,
  getPromiseData,
  getBlogWithTeamFunction,
  saveFeedLink,
  fetchFeed,
  postFeedOnFacebook,
  getComment,
  postComment,
  getReaction,
  likePost,
} = require("../controller/controller");

const {
  dropAllUsers,
  dropAllPost,
  dropAllInivtes,
  getRegisterWriterLink,
  removeTeamFromBlog,
  // getRegisterWriterLink2,
} = require("../controller/controller");

const {
  verifyAuthorization,
  verifyAuthorizationForChangingCurrent,
  validateOwnership,
  sendMail,
  sendInvitationResponeMail,
} = require("../middleware/tokenMiddleware");

const router = new KoaRouter();

router.get("/hello", (ctx) => {
  ctx.body = "Hello";
});

router.post("/register", validateUser, registerScw);

// router.get("/login", loginRender);
// router.post("/loginWriter", loginWriter);

router.post("/login", loginUser);

router.get("/registerWriter", getRegisterWriterLink);

router.post(
  "/registerWriter",
  validateWriter,
  sendInvitationResponeMail,
  registerWriter
);

// router.post(
//   "/registerWriter2",
//   validateWriter2,
//   sendInvitationResponeMail,
//   registerWriter
// );

router.post("/createblog", verifyAuthorization, validateBlog, createBlog);

router.delete(
  "/deleteblogbyid",
  verifyAuthorization,
  validateOwnership,
  deleteBlogByBid
);

router.put(
  "/updateBlog",
  verifyAuthorization,
  validateOwnership,
  validateUpdate,
  updateBlogByBid
);

router.get(
  "/getblogbyid",
  verifyAuthorization,
  validateOwnership,
  getBlogByBid
);

// router.post("/deleteteammeber", verifyAuthorization, deleteMyTeam);

router.post(
  "/invite",
  verifyAuthorization,
  inviteValidation,
  sendMail,
  saveInviteDetails
);

router.get("/getMyInvite", verifyAuthorizationForChangingCurrent, getMyInvite);

router.post(
  "/respondToInvite",
  verifyAuthorizationForChangingCurrent,
  acceptInvite,
  sendInvitationResponeMail
);

router.post(
  "/changeMaster",
  verifyAuthorizationForChangingCurrent,
  changeMaster
);

router.post("/leaveteam", verifyAuthorization, leaveTeam);

router.post(
  "/removeteamfromblog",
  verifyAuthorization,
  validateOwnership,
  validateTeamRemoval,
  removeTeamFromBlog
);

router.post("/removefromteam", verifyAuthorization, removeFromMyTeam);

router.delete("/deletemyself", verifyAuthorization, deleteMyself);

router.get(
  "/getCurrentMaster",
  verifyAuthorizationForChangingCurrent,
  getCurrentMaster
);

router.get("/getmymaster", verifyAuthorizationForChangingCurrent, getAllMaster);

router.get("/getpromisedata", getPromiseData);

router.get("/getmyteam", verifyAuthorization, getMyTeam);

router.get(
  "/getMyBlogs",
  verifyAuthorization,
  validateGetMyBlog,
  getAllBlogById
);
router.get("/showallblogs", showAllBlogs);

//////////////////////////////////////////////////////////////////////////
router.delete("/dropallpost", dropAllPost);
router.delete("/dropallusers", dropAllUsers);
router.delete("/dropallinvites", dropAllInivtes);

router.post(
  "/postonfacebook",
  verifyAuthorization,
  validateOwnership,
  postOnFacebook
);

router.get(
  "/getblogwithteam",
  verifyAuthorization,
  validateGetBlogWithTeam,
  getBlogWithTeamFunction
);
router.get("/getteamwithblog", verifyAuthorization, getTeamWithBlogFunction);
///////////////////////////////////////////////////////////////////////

router.post(
  "/addfeed",
  verifyAuthorization,
  validateSavingFeedLink,
  saveFeedLink
);

router.post("/fetchfeed", verifyAuthorization, fetchFeed);

router.post("/postfeed", verifyAuthorization, postFeedOnFacebook);

router.get("/getComment", getComment);
router.post("/postComment", postComment);
router.get("/getReaction", getReaction);
router.post("/likePost", likePost);

router.post("/postOnInstagram");
///////////////////////////////////////////////////////////////////////

module.exports = router;
