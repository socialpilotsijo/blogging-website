// require("dotenv").config({ path: `../.env` });

const db = require("./MongoConnect").db("Blogging");

const inviteCollection = db.collection("invite");

// Insert invite record
const addInvite = async (inviteBody) =>
  await inviteCollection.insertOne(inviteBody);

// Retrieve pending invites on the particular email(email of people who are invited)
const getInvitesByEmail = async (email) =>
  await inviteCollection
    .find({ email, accepted: "pending" })
    .project({ _id: 0, rid: 1, senderEmail: 1, name: 1, company: 1 })
    .toArray();

const deleteBlogFromInvite = async (bid) =>
  await inviteCollection.updateMany(
    { blogList: bid },
    { $pull: { blogList: bid } }
  );

// Delete all the invite sent by one uid
const deleteAllInviteByUid = async (uid) =>
  await inviteCollection.deleteMany({ uid });

// Retrieve pending invites made by the user(inviter) by uid(inviter uid)
const getInvitesById = async (uid) =>
  await inviteCollection
    .find({ uid, accepted: "pending" })
    .project({ _id: 0, rid: 1, email: 1 })
    .toArray();

// get some details of the rid
const getInviteMaster = async (rid) =>
  await inviteCollection.findOne(
    { rid },
    {
      projection: {
        _id: 0,
        uid: 1,
        email: 1,
        senderEmail: 1,
        blogList: 1,
        accepted: 1,
      },
    }
  );

// Accept or reject invite by rid
const acceptInviteById = async (rid, accept) => {
  await inviteCollection.updateOne({ rid }, { $set: { accepted: accept } });
};

const deleteAllInviteByEmail = async (email) =>
  await inviteCollection.deleteMany({ email });

// Accept or reject all the invites by uid and email
const acceptInviteTable = async (uid, email, accept) => {
  const result = await inviteCollection.updateMany(
    { uid, email },
    { $set: { accepted: accept } }
  );
  return result;
};

//Check if the invite already exist
const getRidifInviteAlreadyExistAndPending = async (uid, email) =>
  await inviteCollection.findOne(
    { uid, email: email, accepted: "pending" },
    { projection: { rid: 1, blogList: 1 } }
  );

// Check if the invite exist anymore
const inviteExistByRid = async (rid) => {
  const result = await inviteCollection.findOne({ rid });
  return result !== null;
};

// If the invite is already responded to
const isInviteAcceptedOrRejected = async (rid) => {
  // Note:- Please check if the rid already exist before using this
  const result = await inviteCollection.findOne({ rid, accepted: "pending" });
  return result === null;
};

const updateBlogListInInvite = async (rid, blogList) =>
  await inviteCollection.updateOne(
    { rid },
    { $addToSet: { blogList: { $each: blogList } } }
  );

// Get blogList by rid
const getBlogListByRid = async (rid) => await inviteCollection.findOne({ rid });

const dropallinvites = async () => await inviteCollection.drop();

//////////////////////////////////////////////////////////////////////////////////

module.exports = {
  addInvite,
  deleteAllInviteByUid,
  deleteAllInviteByEmail,
  getBlogListByRid,
  inviteExistByRid,
  isInviteAcceptedOrRejected,
  getInvitesByEmail,
  getInvitesById,
  getInviteMaster,
  deleteBlogFromInvite,
  getRidifInviteAlreadyExistAndPending,
  acceptInviteById,
  acceptInviteTable,
  updateBlogListInInvite,
  dropallinvites,
};
