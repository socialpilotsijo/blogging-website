const db = require("./MongoConnect").db("Blogging");

const PostCollection = db.collection("posts");

const insertOnePost = async (post) => await PostCollection.insertOne(post);

module.exports = { insertOnePost };
