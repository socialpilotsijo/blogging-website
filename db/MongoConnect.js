// require("dotenv").config({ path: "../.env" });

const { MongoClient } = require("mongodb");

// const connectionUrl = process.env.DB_URL;
const connectionUrl = "mongodb://localhost:27017";

const client = new MongoClient(connectionUrl, {
  useUnifiedTopology: true,
});

client.connect((err) => {
  if (err) {
    return console.log("Connection failed", err);
  }
  console.log("Connection successful");
});

module.exports = client;
