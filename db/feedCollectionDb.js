const db = require("./MongoConnect").db("Blogging");

const feedCollection = db.collection("feeds");

const saveFeedDataDb = async (feed) => await feedCollection.insertOne(feed);

const getFeedDynamically = async (matchBy, projectBy) =>
  await feedCollection.find(matchBy).project(projectBy).toArray();

const updateLatestFeedList = async (fid, latestFeed) =>
  await feedCollection.updateOne(
    { fid },
    { $set: { latestFeed, current: "ready" } }
  );

const updateFeedCurrent = async (fid, current) =>
  await feedCollection.updateOne({ fid }, { $set: { current } });

const updateSingleFeedDynamically = async (matchBy, updateItem) =>
  await feedCollection.updateOne(matchBy, updateItem);

module.exports = {
  saveFeedDataDb,
  getFeedDynamically,
  updateLatestFeedList,
  updateFeedCurrent,
  updateSingleFeedDynamically,
};
