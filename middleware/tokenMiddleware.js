// require("dotenv").config({ path: "../.env" });
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");

const {
  isUserExistById,
  checkIfCurrentIsValid,
  checkIfMasterIsValid,
} = require("../db/UserCollectionDb");

const { findBlogByBid } = require("../db/BlogCollectionDb");

const createToken = (uid, email, rights) =>
  jwt.sign({ uid, email, rights }, process.env.SECRET_KEY);

const createTokenForWriter = (uid, email, rights, current) =>
  jwt.sign({ uid, email, rights, current }, process.env.SECRET_KEY);

const verifyAuthorization = async (ctx, next) => {
  try {
    const authorizeHeader = ctx.request.header.authorization;
    if (authorizeHeader) {
      const bearerHead = authorizeHeader.split(" ");
      const token = bearerHead[1];
      // ctx.state.token = token;
      const verifyWithVal = jwt.verify(token, process.env.SECRET_KEY);
      // const uid = verifyWithVal["uid"];
      // const email = verifyWithVal["email"];
      // const rights = verifyWithVal["rights"];

      const { uid, email, rights } = verifyWithVal;
      //A temporary check of user exist or not
      await userExist(uid);

      if (rights === 2) {
        const current = verifyWithVal["current"];
        // If the current is included in the masterlist
        const isMasterValid = await checkIfMasterIsValid(uid, current);
        if (!isMasterValid) throw new Error("Owner not valid");

        const isCurrentValid = await checkIfCurrentIsValid(uid, current);

        if (!isCurrentValid)
          throw new Error("Your current owner is someone else");

        ctx.state.current = current;
      }

      // ctx.state.uid = uid;
      // ctx.state.email = email;
      // ctx.state.rights = rights;

      Object.assign(ctx.state, { uid, email, rights });

      await next(ctx);
    } else {
      throw new Error("User not logged in");
    }
  } catch (err) {
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const verifyAuthorizationForChangingCurrent = async (ctx, next) => {
  try {
    const authorizeHeader = ctx.request.header.authorization;
    if (authorizeHeader) {
      const bearerHead = authorizeHeader.split(" ");
      const token = bearerHead[1];
      // ctx.state.token = token;
      const verifyWithVal = jwt.verify(token, process.env.SECRET_KEY);
      // const uid = verifyWithVal["uid"];
      // const email = verifyWithVal["email"];
      // const rights = verifyWithVal["rights"];

      const { uid, email, rights } = verifyWithVal;

      //A temporary check of user exist or not

      await userExist(uid);

      if (rights === 2) {
        const current = verifyWithVal["current"];
        // If the current is included in the masterlist
        if (current) {
          const isMasterValid = await checkIfMasterIsValid(uid, current);
          if (!isMasterValid) throw new Error("Owner not valid");

          const isCurrentValid = await checkIfCurrentIsValid(uid, current);
          if (!isCurrentValid)
            throw new Error("Your current owner is someone else");
        }

        ctx.state.current = current;
      }

      // ctx.state.uid = uid;
      // ctx.state.email = email;
      // ctx.state.rights = rights;

      Object.assign(ctx.state, { uid, email, rights });

      await next(ctx);
    } else {
      throw new Error("User not logged in");
    }
  } catch (err) {
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const userExist = async (uid) => {
  if (!(await isUserExistById(uid)))
    throw new Error("This user no longer exist");
};

// const validateRights = async (ctx, next) => {
//   const uid = ctx.state.uid;
//   const getRight = await getRights(uid);

//   if (getRight["rights"] === 1) {
//     ctx.state.rights = 1;
//   } else {
//     ctx.state.rights = 2;
//   }
//   await next(ctx);
// };

const validateOwnership = async (ctx, next) => {
  try {
    const rights = ctx.state.rights;
    const bid = ctx.query.bid;
    const uid = ctx.state.uid;
    ctx.state.bid = bid;
    console.log(bid);
    const blog = await findBlogByBid(bid);
    if (blog === null) throw new Error("No such blog exist");
    else {
      if (rights === 1) {
        if (blog["uid"] !== uid)
          throw new Error(
            "You are not authorized to access or manipulate the blog"
          );
        else await next(ctx);
      } else {
        // Check if the uid exist in team (or the team array exist) of blog
        if (!blog["team"] || !blog["team"].includes(uid))
          throw new Error(
            "You are not authorized to access or manipulate the blog"
          );
        else await next(ctx);
      }
    }
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const sendMail = async (ctx, next) => {
  try {
    const inviteUser = ctx.state.inviteUser;

    //Checking if already in team flag from previous middleware
    if (inviteUser["isAlreadyInMyTeamFlag"]) {
      await next(ctx);
      return;
    }
    const {
      name,
      company,
      logo,
      email: receiverMail,
      senderEmail: senderMail,
      exists: userExist,
      blogList,
      rid,
    } = inviteUser;
    // const name = inviteUser["name"];
    // const company = inviteUser["company"];
    // const logo = inviteUser["logo"];
    // const receiverMail = inviteUser["email"];
    // const senderMail = inviteUser["senderEmail"];
    // const userExist = inviteUser["exists"];
    // const blogList = inviteUser["blogList"];
    // const rid = inviteUser["rid"];
    const mid = ctx.state.uid;

    const secretLink = jwt.sign(
      { email: receiverMail, mid, rid },
      process.env.SECRET_KEY
    );

    let text;
    let inviteLink;
    if (!userExist) {
      text = `You have been invited by ${name} to join company ${company}. Get in touch with them using ${senderMail}. Register`;
      inviteLink = `localhost:3000/registerWriter?data=${secretLink}`;
    } else {
      text = `You have been invited by ${name} to join company ${company}. Get in touch with them using ${senderMail}. Login`;
      inviteLink = `localhost:3000/login?email=${receiverMail}`;
    }
    delete inviteUser["exists"];

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.email_uname,
        pass: process.env.email_pwd,
      },
    });

    const mailOptions = {
      from: process.env.email_uname, // sender address
      to: `${receiverMail}`, // list of receivers
      subject: "Invitation Email",
      text: `${text} ${inviteLink}`,
      html: `<head>
                <style>
                  .btnLink {
                    background: none!important;
                    border: none;
                    padding: 0!important;
                    /*optional*/
                    font-family: arial, sans-serif;
                    /*input has OS specific font-family*/
                    color: #069;
                    text-decoration: underline;
                    cursor: pointer;
                  }

                  .card-div {
                    background:yellow;
                    height:600px;
                    width: 600px;
                    align:center;
                  }
                </style>
              </head>
              <body>
              <div class="card-div"><img src=${logo} alt="Company logo" width="30px", height="30px"> 
                  <h1> ${text} <a href="http://${inviteLink}"> here </a> </h1>
              </body>`,
    };

    await transporter
      .sendMail(mailOptions)
      .then(() => console.log("Email sent"))
      .catch((err) => console.log("error occured", err));

    await next(ctx);
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

const sendInvitationResponeMail = async (ctx, next) => {
  try {
    // const emailResponse = ctx.state.emailResponse;
    // const accepted = emailResponse["accepted"];
    // const senderEmail = emailResponse["senderEmail"];
    // const email = emailResponse["email"];

    const { accepted, senderEmail, email } = ctx.state.emailResponse;

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.email_uname,
        pass: process.env.email_pwd,
      },
    });

    const mailOptions = {
      from: process.env.email_uname, // sender address
      to: `${senderEmail}`, // list of receivers
      subject: "Invitation Response",
      text: `Your invitation has been ${accepted} by ${email}`,
    };

    // transporter.sendMail(mailOptions, function (error, info) {
    //   if (error) {
    //     console.log(error);
    //   } else {
    //     console.log("Email sent: " + info.response);
    //   }
    // });

    await transporter
      .sendMail(mailOptions)
      .then(() => console.log("Email sent"))
      .catch((err) => console.log("error occured", err));
    await next(ctx);
  } catch (err) {
    console.log("Error occured", err);
    ctx.status = 400;
    ctx.body = {
      msg: "An error occured",
      error: err.message,
    };
  }
};

module.exports = {
  createToken,
  createTokenForWriter,
  verifyAuthorization,
  verifyAuthorizationForChangingCurrent,
  validateOwnership,
  sendMail,
  sendInvitationResponeMail,
};
